# Migrations without network congestion #

This code perform migrations of virtual machines individually one by one to study the effect of network congestion in the duration of the migration process.

The .csv input file should be in the following form:

``` number of cpu cores of the VM; vm id; source host ; destination host; duration of the migration; estimation of the duration  ```

The .csv output file will be in the following form:

``` vm id; time it took to migrate only this vm  ;  estimation of the duration ; duration of the migration (from the input); source host ; destination host; number of cpu cores of the VM; simulation time ```

